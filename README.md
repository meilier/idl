# idl

#### 介绍
image dependency list —— idl
用于分析系统容器镜像内各软件包依赖关系，适用带有yum/dnf包管理器的系统。例如，openEuler,，CentOS等。

#### 软件架构
* query.py dnf查询接口封装
* idl.py 软件主程序
* dot.py 生成依赖关系dot、png文件


#### 安装教程

1.  安装 python3
以centos为例子：
```
yum install python3

```
2.  安装 graphviz 以支持依赖图生成

```
yum install -y graphviz
```

3.  安装 graphviz python3接口
```
pip3 install graphviz
```



#### 使用说明

1.  查看`coreutils`软件包依赖（centos8-x86_64.conf为dnf包管理器配置文件，包含centos8软件源）：

`python3 idl.py conf/centos8-x86_64.conf -p coreutils`

2.  查看`centos8`基础镜像入口依赖（centos8.list为centos8基础镜像内软件包列表）：

`python3 idl.py conf/centos8-x86_64.conf -e image_package_list/centos8.list`

3.  生成`coreutils`包依赖及依赖图

`python3 idl.py conf/centos8-x86_64.conf -p coreutils -g`

![coreutils软件包依赖图](./example/deps.dot.png)

4.  在`libcurl`生成的依赖关系网中去除某一软件包`krb5-libs`引入的依赖

`python3 idl.py conf/openeuler20.03-sp2-x86_64.conf -p libcurl -sd -rt krb5-libs -colored`
![libcurl去除krb5-libs软件包依赖图](./example/deps-colored.dot.png)

5. 其他帮助

`python3 idl.py -h`

```shell
usage: idl.py [-h] [-w] [-arch [ARCHITECTURE]] [-e [ENTRY_POINT_PACKAGE]] [-v]
              [-p PACKAGE [PACKAGE ...]] [-sd] [-g]
              [-rt REMOVE_TEST [REMOVE_TEST ...]]
              source

image dependency list tool

positional arguments:
  source                source repository configuration file

optional arguments:
  -h, --help            show this help message and exit
  -w, --weak_dependency_enabled
                        enable weak dependency check
  -arch [ARCHITECTURE], --architecture [ARCHITECTURE]
                        image architecture default is platform.machine()
  -e [ENTRY_POINT_PACKAGE], --entry_point_package [ENTRY_POINT_PACKAGE]
                        show entry point package of an image
  -v, --verbose         show verbose log
  -p PACKAGE [PACKAGE ...], --package PACKAGE [PACKAGE ...]
                        pakcage to analyse
  -sd, --show_dependency_dict
                        show dependency dict (default show package list)
  -g, --generate_dot_and_png
                        generate dependency dot and png file
  -rt REMOVE_TEST [REMOVE_TEST ...], --remove-test REMOVE_TEST [REMOVE_TEST ...]
                        remove test for current dependency dot
  -colored, --colored_remove_test
                        generate dependency dot and png file of colored
                        packages in remove-test
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
