from graphviz import Digraph
import graphviz


class ImageDot:
    def __init__(self):
        self.g = Digraph('G', filename='deps.dot')

    def generate_dot(self, depends_dict, colored, all_dict):
        if not colored:
            for k, v in depends_dict.items():
                for p in v:
                    self.g.edge(k, p)
            return

        removed_package = []
        for k, v in all_dict.items():
            if k not in depends_dict:
                removed_package.append(k)
                self.g.node(k, fillcolor="/pastel19/2", style="rounded,filled")
            for p in v:
                self.g.edge(k, p)
        print("removed packages are:", removed_package)
        print
    def save(self):
        self.g.render(filename='deps.dot', format='png')
